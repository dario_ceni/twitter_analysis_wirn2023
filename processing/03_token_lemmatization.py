from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize, sent_tokenize
import pandas as pd
import time



text_tokenizeAndLemmatize = []
text_Lemmatize = []
text_tokenize = []
def TokAndLem(tweets):
    text_tokenizeAndLemmatize = []
    for tweet in tweets:
        text_Lemmatize = []
        text_tokenize = []
        text_tokenize = word_tokenize(tweet)
        lemmatizer = WordNetLemmatizer()
        for i in text_tokenize:
            text_Lemmatize.append(lemmatizer.lemmatize(i, 'v'))

        text_tokenizeAndLemmatize.append(' '.join(text_Lemmatize))
        #print(text_tokenizeAndLemmatize)

    return text_tokenizeAndLemmatize



nomeCorrenteDataset = "dataset_" + time.strftime("%d_%m_%Y")
# nomeCorrenteDataset = "all_at_18_05_2023"
df = pd.read_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
df['text_clean_EN_TokAndLem'] = pd.DataFrame(TokAndLem(df['text_clean_EN']))
df.to_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv')