import pandas as pd
#import deepl
from time import sleep
import re, string
from cleantext import clean
from googletrans import Translator
import time
from concurrent.futures import ThreadPoolExecutor
import random


# import datset
nomeCorrenteDataset = "dataset_" + time.strftime("%d_%m_%Y")
# nomeCorrenteDataset = "all_at_18_05_2023"
df = pd.read_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
#df = df.head(35)


"""
###########################################using deepl free trial#########################################################################
"""
#translator = deepl.Translator("9d14b430-33aa-bdd5-18d8-6d4b4acbffdc:fx")
#
## qui è stato sostituito l'uso di index con i... il problema è che non elimina e duplicati e quindi non traduceva messaggi uguali
#for i, tweet in enumerate(df.text_clean_IT):
#    #tweet_index = df[df['text_clean_IT'] == tweet].index.values.astype(int)[0]
#    df.at[i, 'translated_tweets'] = translator.translate_text(str(tweet), target_lang="EN-US")
#    #print(tweet_index,str(tweet),translator.translate_text(str(tweet), target_lang="EN-US"))
#
#df.to_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv')
"""
###########################################using deepl free trial#########################################################################
"""








"""
########################################### using googletrans ####################################################################
"""
translator = Translator()
dflist = df.text_clean_IT.tolist()
nworker = 25
nelem = int(len(dflist) / nworker)
j = 0
for i in range(0, nworker):
    # print(dflist[i*nworker:(i+1)*nworker])
    s = time.time()
    if i != nworker - 1:
        print(i*nelem, (i+1)*nelem)
        trans = translator.translate(dflist[i*nelem:(i+1)*nelem], dest='en')
    else:
        print(i*nelem, "infinito")
        trans = translator.translate(dflist[i*nelem:], dest='en')
    print(time.time() - s, i)
    for x in trans:
        print(x.text)
        df.at[j, 'text_clean_EN'] = x.text
        j += 1
"""
########################################### using googletrans ####################################################################
"""


# export df
df.to_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv')


