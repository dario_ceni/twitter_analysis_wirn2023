# post processing vario ai dataset
import pandas as pd
import time
from datetime import datetime, timedelta


def merge_vader_roberta_sentiment():
    """
    ##########################################################################################################
    # pipeline per creare a partire da 2 dataset separati un unico csv con i 2 modelli di sentiment analysis
    # da eseguire dopo aver runnato vader e xlm e avere i due dataset seprati in vader/dataset.. e XLM-roBERTa/dataset...
    ##########################################################################################################
    """
    # definiamo il dataset sul quale operare
    nomeCorrenteDataset = "dataset_21_05_2023"
    df_roberta = pd.read_csv(f'../data/sentiment_analysis/XLM-roBERTa/{nomeCorrenteDataset}.csv', index_col=[0])
    df_vader = pd.read_csv(f'../data/sentiment_analysis/vader/{nomeCorrenteDataset}.csv', index_col=[0])


    df_roberta['vader_SCORE_pnn'] = df_vader['vader_SCORE_pnn']
    df_roberta['vader_SCORE_pnn_numeric'] = df_vader['vader_SCORE_pnn_numeric']
    df_roberta['vader_score_compound'] = df_vader['vader_score_compound']
    df_roberta.to_csv(f'../data/sentiment_analysis/all_model/{nomeCorrenteDataset}.csv')
    """
    ##########################################################################################################
    # pipeline per creare a partire da 2 dataset separati un unico csv con i 2 modelli di sentiment analysis
    ##########################################################################################################
    """


def createCompleteDataset():
    """
    ##########################################################################################################
    # pipeline per unire i diversi dataset relativi a diverse sentiment analysis (predicted e true)
    ##########################################################################################################
    """

    nomeCorrenteDataset = "dataset_from_06_03_to_02_04"
    df = pd.read_csv(f'../data/sentiment_analysis/all_model/{nomeCorrenteDataset}.csv', index_col=[0])

    df['actual_sentiment'] = ""
    manual_labelling = "manual_labelling"
    dfm = pd.read_csv(f'../sentiment_analysis/evaluation/{manual_labelling}.csv')


    for i, row in dfm.iterrows():
        if row['sentimento_chiaro_o_idk'] == 'c':
            df.at[row['tweet_index'], 'actual_sentiment'] = row['sentiment_of_tweet_PNN']


    df.to_csv(f'../data/complete_dataset/{nomeCorrenteDataset}.csv')


    # df = pd.read_csv(f'../data/topic_analysis/dataset_from_06_03_to_02_04/topic_metrics.csv', index_col=[0])
    # df.drop("xml_roBERTA_SCORE",axis=1, inplace=True)


    """
    ##########################################################################################################
    # pipeline per unire i diversi dataset relativi a diverse sentiment analysis
    ##########################################################################################################
    """


def mergeOriginal_dataset():
    """
    ##########################################################################################################
    # pipeline per creare a partire da n dataset in original_dataset, un unico dataset aggregato
    ##########################################################################################################
    """
    # definiamo i dataset sui quali operare

    df1 = pd.read_csv(f'../data/clear_dataset/dataset_21_05_2023.csv', index_col=[0])
    df2 = pd.read_csv(f'../data/clear_dataset/all_at_18_05_2023.csv.csv', index_col=[0])

    frames = [df1, df2]
    all_tweet_dataset = pd.concat(frames)
    all_tweet_dataset = all_tweet_dataset.drop_duplicates(subset='text', keep="first")
    all_tweet_dataset.reset_index(drop=True, inplace=True)

    nomeCorrenteDataset = "all_at_" + time.strftime("%d_%m_%Y")
    all_tweet_dataset.to_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv')


def merge_all_at_with_new_data():
    """
    ##########################################################################################################
    # pipeline per ...
    ##########################################################################################################
    """
    # definiamo il dataset sul quale operare
    nomeDataset_all = "all_at_18_05_2023"
    nomeDataset_new = "dataset_21_05_2023"
    df_all = pd.read_csv(f'../data/sentiment_analysis/all_model/{nomeDataset_all}.csv', index_col=[0])
    df_new = pd.read_csv(f'../data/sentiment_analysis/all_model/{nomeDataset_new}.csv', index_col=[0])

    frames = [df_all, df_new]
    df_all = pd.concat(frames)
    df_all = df_all.drop_duplicates(subset='text', keep="first")
    df_all.reset_index(drop=True, inplace=True)

    # nomeCorrenteDataset = "all_at_" + time.strftime("%d_%m_%Y")
    nomeCorrenteDataset = "all_at_21_05_2023"
    df_all.to_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv')
    """
    ##########################################################################################################
    # pipeline per ...
    ##########################################################################################################
    """

def create_last_month_tweets_dataset():
    data = pd.read_csv('../data/complete_dataset/all_at_21_05_2023.csv', index_col=[0])
    data['created_at'] = pd.to_datetime(data['created_at'])
    end_date = data['created_at'].max().date()  # Get the maximum date in the 'created_at' column
    start_date = end_date - timedelta(days=30)  # Subtract 30 days from the end date
    lastMonth_data = data[(data['created_at'].dt.date >= start_date) & (data['created_at'].dt.date <= end_date)]
    lastMonth_data.to_csv(f'../data/complete_dataset/lastMonth_data.csv')

def create_last_week_tweets_dataset():
    data = pd.read_csv('../data/complete_dataset/all_at_21_05_2023.csv', index_col=[0])
    data['created_at'] = pd.to_datetime(data['created_at'])
    end_date = data['created_at'].max().date()  # Get the maximum date in the 'created_at' column
    start_date = end_date - timedelta(days=6)  # Subtract 30 days from the end date
    last_week_data = data[(data['created_at'].dt.date >= start_date) & (data['created_at'].dt.date <= end_date)]
    last_week_data.to_csv(f'../data/complete_dataset/last_week_data.csv')


# merge_vader_roberta_sentiment()
# createCompleteDataset()
# mergeOriginal_dataset()
# merge_all_at_with_new_data()
# create_last_month_tweets_dataset()
create_last_week_tweets_dataset()

