import pandas as pd
import sys
import get_tweets
from get_tweets import querys
import time



word_in_all_querys = querys.word_in_all_querys_en
def removeQueryWords(df_text):
    dataframeWithOutQueryWord = []

    for currentTweet in df_text:


        for i in word_in_all_querys:
            if i in currentTweet:
               currentTweet = currentTweet.replace(i,"")


        dataframeWithOutQueryWord.append(currentTweet)

    # print(dataframeWithOutQueryWord)
    return dataframeWithOutQueryWord


nomeCorrenteDataset = "dataset_" + time.strftime("%d_%m_%Y")
# nomeCorrenteDataset = "all_at_18_05_2023"
df = pd.read_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
df['text_clean_EN_TokAndLem_WO_query_words'] = pd.DataFrame(removeQueryWords(df['text_clean_EN_TokAndLem']))
df.to_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv')
