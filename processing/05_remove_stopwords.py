# questo processing è pensato per poi applicare il topic modeling a tweets in inglese
import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import pandas as pd
nltk.download('stopwords')
nltk.download('punkt')
stop_words = stopwords.words('english')
import time




def clean_text(x):
  x = str(x)
  x = x.lower()
  x = re.sub(r'#[A-Za-z0-9]*', ' ', x)
  x = re.sub(r'https*://.*', ' ', x)
  x = re.sub(r'@[A-Za-z0-9]+', ' ', x)
  tokens = word_tokenize(x)
  x = ' '.join([w for w in tokens if not w.lower() in stop_words])
  x = re.sub(r'[%s]' % re.escape('!"#$%&\()*+,-./:;<=>?@[\\]^_`{|}~“…”’'), ' ', x)
  x = re.sub(r'\d+', ' ', x)
  x = re.sub(r'\n+', ' ', x)
  x = re.sub(r'\s{2,}', ' ', x)
  return x


#definiamo il dataset sul quale operare (in questo caso tipicamente usaremo clear_dataset/"translated_tweets")
nomeCorrenteDataset = "dataset_" + time.strftime("%d_%m_%Y")
# nomeCorrenteDataset = "all_at_18_05_2023"
df = pd.read_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
df['text_clean_EN_TokAndLem_WO_query_words_WO_stopwords'] = df.text_clean_EN_TokAndLem_WO_query_words.apply(clean_text)
df.to_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv')
# print(df.head())
