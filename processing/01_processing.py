import pandas as pd
pd.options.mode.chained_assignment = None
import string
from cleantext import clean
import nltk
nltk.download('all')
import re, string
from nltk.corpus import stopwords
import ast
import time



# Pulizia dei tweets


# Remove punctuations, links, mentions and \r\n new line characters
def strip_all_entities(text):
    # print(text)
    text = text.rstrip('\"')
    text = text.lstrip('\"')
    text = text.replace('"', '')
    text = text.replace('\r', '').replace('\n', ' ').replace('\n', ' ').lower()  # remove \n and \r and lowercase
    text = re.sub(r"(?:\@|https?\://)\S+", "", text)  # remove links and mentions
    #text = re.sub(r'[^\x00-\x7f]', r'', text)  # remove non utf8/ascii characters such as '\x9a\x91\x97\x9a\x97'
    text = clean(text, no_emoji=True)
    # print(text)
    #banned_list = string.punctuation + 'Ã' + '±' + 'ã' + '¼' + 'â' + '»' + '§'
    #table = str.maketrans('', '', banned_list)
    #text = text.translate(table)
    # print(text)
    return text



# clean hashtags at the end of the sentence, and keep those in the middle of the sentence by removing just the # symbol
def clean_hashtags(tweet):
    new_tweet = " ".join(word.strip() for word in
                         re.split('#(?!(?:hashtag)\b)[\w-]+(?=(?:\s+#[\w-]+)*\s*$)', tweet))  # remove last hashtags
    new_tweet2 = " ".join(word.strip() for word in
                          re.split('#|_', new_tweet))  # remove hashtags symbol from words in the middle of the sentence
    return new_tweet2



# Filter special characters such as & and $ present in some words
def filter_chars(a):
    sent = []
    for word in a.split(' '):
        if ('$' in word) | ('&' in word):
            sent.append('')
        else:
            sent.append(word)
    return ' '.join(sent)



## remove multiple spaces
def remove_mult_spaces(text):
    return re.sub("\s\s+", " ", text)



## remove spam
def remove_spam(text):
    match = re.search(r'subscribe', text)
    if match:
        return ''
    else:
        return text



# rimozione stopwords (ora settato in inglese ma si può settare in italiano)
stop_words = stopwords.words('english')
stop_words += list(string.punctuation)

def remove_stopwords(data):
    newData = data.split()
    filtered_words = [w for w in newData if w not in stop_words]
    original_nostop = ' '.join(filtered_words)
    return original_nostop



texts_clean = []
def generalPreprocess(df_text):
    for t in df_text:
        # print(t, type(t))
        texts_clean.append(strip_all_entities((remove_spam(remove_mult_spaces(filter_chars(clean_hashtags(t)))).lower())))
        # texts_clean.append(strip_all_entities(t).lower())


    print(texts_clean)
    return texts_clean




if __name__ == '__main__':

    #import del dataset
    nomeCorrenteDataset = "dataset_" + time.strftime("%d_%m_%Y")
    # nomeCorrenteDataset = "all_at_18_05_2023"
    df = pd.read_csv(f'data/original_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
    df['text_clean_IT'] = pd.DataFrame(generalPreprocess(df['text']))
    df = df.drop_duplicates(subset='text_clean_IT', keep="first")
    df.reset_index(drop=True, inplace=True)
    # df.drop("Unnamed: 0",axis=1, inplace=True)

    df.to_csv(f'data/clear_dataset/{nomeCorrenteDataset}.csv')

