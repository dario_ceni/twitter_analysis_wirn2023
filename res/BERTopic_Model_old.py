#https://towardsdatascience.com/implement-your-topic-modeling-using-the-bertopic-library-d6708baa78fe
import numpy as np
import pandas as pd
from bertopic import BERTopic


#definiamo il dataset sul quale operare
nomeCorrenteDataset = "dataset_from_06_03_to_02_04"
df = pd.read_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
# df = df.head(5)


tweets = df.translated_tweets_and_clean.to_list()
tweets = [str(x) for x in tweets]

# for i in tweets:
#     print(type(i))

#qui chiamiamo il modello e facciomo l'analisi topic
topic_model = BERTopic(language="english")
topics, probs = topic_model.fit_transform(tweets)


# display topic "intertopic distance map"
# fig = topic_model.visualize_topics()
# fig.write_html(f"../data/topic_analysis/dataset_from_06_03_to_02_04/{nomeCorrenteDataset}BERTopic_map.html")


# display topic "topic word scores"
fig = topic_model.visualize_barchart()
fig.write_html(f"../data/topic_analysis/dataset_from_06_03_to_02_04/{nomeCorrenteDataset}BERTopic_words.html")
print(topic_model.get_topic_info())
