from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import pandas as pd
from numba import jit, cuda
import numpy as np



nomeCorrenteDataset = "dataset_21_05_2023"
df = pd.read_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
#df = df.head(50)
#df["vader"] = " "

# qui eliminiamo dal dataset usato per vader sentiment analysis i tweet labellati manualmente, tuttavia qualcosa non funziona
# poichè ne rimuove 96 e non 99



analyzer = SentimentIntensityAnalyzer()

# in origine prendeva i tweets tradotti ma non processati (cioè tokenizzati e lemmatizzati)
for i, tweet in enumerate(df.text_clean_EN_TokAndLem_WO_query_words_WO_stopwords):
    print(i)
    score = "{}".format(str(analyzer.polarity_scores(str(tweet))))
    score = score.partition("'compound': ")[2]
    score = float(score[:-1])

    print(score)
    df.at[i, 'vader_score_compound'] = score
    if score >= 0.05:
        df.at[i, 'vader_SCORE_pnn'] = "positive"
        df.at[i, 'vader_SCORE_pnn_numeric'] = 1
    elif score > -0.05 and score < 0.05:
        df.at[i, 'vader_SCORE_pnn'] = "neutral"
        df.at[i, 'vader_SCORE_pnn_numeric'] = 0
    elif score <= -0.05:
        df.at[i, 'vader_SCORE_pnn'] = "negative"
        df.at[i, 'vader_SCORE_pnn_numeric'] = -1

    if score >= 0.0:
        df.at[i, 'vader_SCORE_pn'] = "positive"
        df.at[i, 'vader_SCORE_pn_numeric'] = 1
    else:
        df.at[i, 'vader_SCORE_pn'] = "negative"
        df.at[i, 'vader_SCORE_pn_numeric'] = -1




df.to_csv(f'../data/sentiment_analysis/vader/{nomeCorrenteDataset}.csv')
# df.to_csv(f'../data/sentiment_analysis/all_model/{nomeCorrenteDataset}.csv')