from feel_it import EmotionClassifier, SentimentClassifier
import pandas as pd
import numpy as np
from time import time

sentimenti = {"negative": -1, "neutral": 0, "positive": 1}

#definiamo il dataset sul quale operare
nomeCorrenteDataset = "dataset_from_06_03_to_02_04"
df = pd.read_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
# df = df.head(50)


# creiamo la colonna nella quale metteremo la sentiment analisi con feel-it
df["feel_it_SCORE"] = " "
df["feel_it_SCORE_numeric"] = " "
sentiment_classifier = SentimentClassifier()


def sentiment_with_feelit():
    for i, tweet in enumerate(df.text_clean):
        sent = sentiment_classifier.predict([str(tweet)])
        df.at[i, 'feel_it_SCORE'] = sent[0]
        df.at[i, 'feel_it_SCORE_numeric'] = sentimenti[sent[0]]
        print(f"[{i}] {str(tweet)}: {sent}")

# print(type(df.text_clean.tolist()[0]))
sentiment_with_feelit()


df.to_csv(f'../data/sentiment_analysis/feel_it/{nomeCorrenteDataset}.csv')







