import tensorflow as tf
from transformers import AutoTokenizer, AutoModel, TFAutoModel, AutoConfig
from transformers import pipeline
import pandas as pd
import torch
import time
from torch.quantization import get_default_qconfig, quantize
import numpy as np
from numba import jit, cuda
from transformers import AutoModelForSequenceClassification
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer, AutoConfig
import numpy as np
from scipy.special import softmax
# si può migliorare lo script rimuovendo il for i e passando una lista di n tweets



sentimenti = {"negative": -1, "neutral": 0, "positive": 1}

nomeCorrenteDataset = "dataset_21_05_2023"
df = pd.read_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
# df = df.head(50)


"""
# metodo 1 per
"""
# MODEL = "cardiffnlp/twitter-xlm-roberta-base-sentiment"
# tokenizer = AutoTokenizer.from_pretrained(MODEL)
# config = AutoConfig.from_pretrained(MODEL)
#
#
# model = AutoModelForSequenceClassification.from_pretrained(MODEL)
# model.save_pretrained(MODEL)
#
#
# def sentiment_with_xlmroberta():
#     for i, tweet in enumerate(df.text_clean_IT):
#         encoded_input = tokenizer(tweet, return_tensors='pt')
#         output = model(**encoded_input)
#         scores = output[0][0].detach().numpy()
#         scores = softmax(scores)
#         print(i, scores)
#
#
# sentiment_with_xlmroberta()
"""
# metodo 1 per
"""


"""
# metodo 2 per
"""
# model_path = "cardiffnlp/twitter-xlm-roberta-base-sentiment"
# sentiment_task = pipeline("sentiment-analysis", model=model_path, tokenizer=model_path)
# #
# df["xlm_roberta_SCORE"] = " "
# df["xlm_roberta_SCORE_confidence"] = " "
#
# def sentiment_with_xlmroberta():
#     for i, tweet in enumerate(df.text_clean_IT):
#         score = sentiment_task(str(tweet))
#         df.at[i, 'xlm_roberta_SCORE'] = score[0]['label']
#         df.at[i, 'xlm_roberta_SCORE_numeric'] = sentimenti[score[0]['label']]
#         df.at[i, 'xlm_roberta_SCORE_confidence'] = score[0]['score']
#         # print(i, score[0]['label'])
#         print(i, score[0]['score'])
#
#
# sentiment_with_xlmroberta()
"""
# metodo 2 per
"""



"""
# metodo 3 per Per ora è quello che funziona meglio
"""
import requests

dflist = df.text_clean_IT.tolist()
j = 0

API_URL = "https://api-inference.huggingface.co/models/cardiffnlp/twitter-xlm-roberta-base-sentiment"
headers = {"Authorization": f"Bearer {'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'}"}


def query(payload):
    response = requests.post(API_URL, headers=headers, json=payload)
    return response.json()


def split_list(lst, n):
    return [lst[i::n] for i in range(n)]

dflist = split_list(dflist, 60)

for sublist in dflist:
    output = query({
                    "inputs": sublist
                    })

    for score in output:
        print(score[0]['label'])
        df.at[j, 'xlm_roberta_SCORE'] = score[0]['label']

        print(sentimenti[score[0]['label']])
        df.at[j, 'xlm_roberta_SCORE_numeric'] = sentimenti[score[0]['label']]

        print(score[0]['score'])
        df.at[j, 'xlm_roberta_SCORE_confidence'] = score[0]['score']
        j += 1


"""
# metodo 3 per
"""

df.to_csv(f'../data/sentiment_analysis/XLM-roBERTa/{nomeCorrenteDataset}.csv')
# df.to_csv(f'../data/sentiment_analysis/all_model/{nomeCorrenteDataset}.csv')
