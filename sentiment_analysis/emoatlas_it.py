import os
# os.system('python -m spacy download en_core_web_lg')
from emoatlas import EmoScores
import pandas as pd
import spacy
spacy.load("it_core_news_sm")



# import datset
nomeCorrenteDataset = "all_at_21_05_2023"
df = pd.read_csv(f'../data/complete_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
# df = df.head(50)
df["emoatlas_IT"] = " "

# definiamo il modello
emos = EmoScores(language="italian")

for i, tweet in enumerate(df.text_clean_IT):
    print(i)
    zscores = emos.zscores(tweet)
    df.at[i, 'emoatlas_IT'] = zscores
    print(zscores)


df.to_csv(f'../data/sentiment_analysis/emoatlas/{nomeCorrenteDataset}.csv')