import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import matthews_corrcoef
import ast




# Qui ci andrà il codice per fare accuracy, f-score etc ##################################################
##########################################################################################################

# import del dataset in cui ci sono le 3 sentiment analysis in codifica numerica (es. -1,1,0)
nomeCorrenteDataset = "all_at_21_05_2023"
df_predicted_complete = pd.read_csv(f'../../data/sentiment_analysis/all_model/{nomeCorrenteDataset}.csv')

# import del dataset (csv) creato giudicando manualmente i tweet e mettendoli in file excel
manual_labelling = "manual_labelling"
df = pd.read_csv(f'{manual_labelling}.csv')



df_true = pd.DataFrame()
df_true['tweet_index'] = df['tweet_index']
# df_true['sentiment_of_tweet_PN'] = df['sentiment_of_tweet_PN']
df_true['sentiment_of_tweet_PNN'] = df['sentiment_of_tweet_PNN']
df_true['sentimento_chiaro_o_idk'] = df['sentimento_chiaro_o_idk']
# df_true['sentiment_to_transition_PN'] = df['sentiment_to_transition_PN']
# df_true['sentiment_to_transition_PNN'] = df['sentiment_to_transition_PNN']

df_true.sort_values(by=['tweet_index'], inplace=True)
# df_true.reset_index()

# df_list_true è il dataset df_true, ovvero quello contenente le valutazioni reali
df_list_true = df_true.values.tolist()
# print(df_list_true)

# scores_list è il dataset prodotto dall'analisi dei modelli feel-it, vader etc
df_predicted = pd.DataFrame()
df_predicted['vader_SCORE_pnn_numeric'] = df_predicted_complete['vader_SCORE_pnn_numeric']
df_predicted['xlm_roberta_SCORE_numeric'] = df_predicted_complete['xlm_roberta_SCORE_numeric']
predicted_list = df_predicted.values.tolist()


for x in df_list_true:
    # in questo modo stiamo aggiugendo alla lista che per ora contiene solo 4 colonne del file excel di valutazione reale
    # anche le colonne dei modelli di sentiment analysis presenti in data/complete_dataset
    x.extend(predicted_list[x[0]][:])
    # print(x)
# print(len(df_list_true))

# qui da sistemare, metto in dataset poichè non è coerente avere una lista che contiene tutto che si chiama true.
# Il fatto è che facciamo un extend alla list df_list_true quando in realtà dovremmo creare una lista ex novo per il dataset completo
# sia di valori predetti che di valori true
dataset = df_list_true


"""
da qui in poi analisi delle metrics f-score, accuracy, MCC
"""


def feelit_PN_c_idk():
    """ non funziona bisogna aggiornare per farlo funzionare anche su feelit
    ####################################################################################################
    # qui facciamo metrics su "feel-it", cioè considerando solo score "positive/negative"## stiamo includendo sia c che idk
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """
    # TP = 0
    # FP = 0
    # TN = 0
    # FN = 0
    #
    # for i in df_list_true:
    #         if i[6] == 1:
    #             if i[1] == 1:
    #                 TP += 1
    #             else:
    #                 FP += 1
    #
    #         elif i[6] == -1:
    #             if i[1] == 1:
    #                 FN += 1
    #             else:
    #                 TN += 1
    #
    # print("TN:", TN," TP:", TP," FP:", FP," FN: ", FN)
    #
    # # Precision Score = TP / (FP + TP)
    # precision_score = TP / (FP + TP)
    # print("feel-it METRICS sia c che idk")
    # print("precision score: ", precision_score)
    #
    # # Recall Score = TP / (FN + TP)
    # recall_score = TP / (FN + TP)
    # print("recall score: ", TP / (FN + TP))
    #
    # # F1 Score = 2* Precision Score * Recall Score/ (Precision Score + Recall Score)
    # print("f1-score: ", 2*precision_score*recall_score / (precision_score + recall_score))
    #
    # # Accuracy Score = (TP + TN)/ (TP + FN + TN + FP)
    # accuracy_score = (TP + TN)/ (TP + FN + TN + FP)
    # print("accuracy score: ", accuracy_score, "\n")
    """
    ####################################################################################################
    # qui facciamo metrics su "feel-it", cioè considerando solo score "positive/negative" stiamo includendo sia c che idk
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """


def feelit_PN_c():
    """
    ####################################################################################################
    # qui facciamo metrics su "feel-it", cioè considerando solo score "positive/negative"## considerando solo c
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """
    # TP = 0
    # FP = 0
    # TN = 0
    # FN = 0
    #
    # for i in df_list_true:
    #     if i[3] == 'c':
    #         if i[6] == 1:
    #             if i[1] == 1:
    #                 TP += 1
    #             else:
    #                 FP += 1
    #
    #         elif i[6] == -1:
    #             if i[1] == 1:
    #                 FN += 1
    #             else:
    #                 TN += 1
    #
    # print("TN:", TN," TP:", TP," FP:", FP," FN: ", FN)
    #
    # # Precision Score = TP / (FP + TP)
    # precision_score = TP / (FP + TP)
    # print("feel-it METRICS, solo c")
    # print("precision score: ", precision_score)
    #
    # # Recall Score = TP / (FN + TP)
    # recall_score = TP / (FN + TP)
    # print("recall score: ", TP / (FN + TP))
    #
    # # F1 Score = 2* Precision Score * Recall Score/ (Precision Score + Recall Score)
    # print("f1-score: ", 2*precision_score*recall_score / (precision_score + recall_score))
    #
    # # Accuracy Score = (TP + TN)/ (TP + FN + TN + FP)
    # accuracy_score = (TP + TN)/ (TP + FN + TN + FP)
    # print("accuracy score: ", accuracy_score, "\n")
    """
    ####################################################################################################
    # qui facciamo metrics su "feelit ", cioè considerando solo score "positive/negative" considerando solo c
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """


def vader_PN_c_idk():
    """
    ####################################################################################################
    # qui facciamo metrics su "vader_PN", cioè considerando solo score "positive/negative" stiamo includendo sia c che idk
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """
    # TP = 0
    # FP = 0
    # TN = 0
    # FN = 0
    #
    # for i in df_list_true:
    #     if i[8] == 1:
    #         if i[1] == 1:
    #             TP += 1
    #         else:
    #             FP += 1
    #
    #     elif i[8] == -1:
    #         if i[1] == 1:
    #             FN += 1
    #         else:
    #             TN += 1
    #
    # print("TN:", TN," TP:", TP," FP:", FP," FN: ", FN)
    #
    # # Precision Score = TP / (FP + TP)
    # precision_score = TP / (FP + TP)
    # print("vader METRICS, sia c che idk")
    # print("precision score: ", precision_score)
    #
    # # Recall Score = TP / (FN + TP)
    # recall_score = TP / (FN + TP)
    # print("recall score: ", TP / (FN + TP))
    #
    # # F1 Score = 2* Precision Score * Recall Score/ (Precision Score + Recall Score)
    # print("f1-score: ", 2*precision_score*recall_score / (precision_score + recall_score))
    #
    # # Accuracy Score = (TP + TN)/ (TP + FN + TN + FP)
    # accuracy_score = (TP + TN)/ (TP + FN + TN + FP)
    # print("accuracy score: ", accuracy_score, "\n")

    """
    ####################################################################################################
    # qui facciamo metrics su "vader_PN", cioè considerando solo score "positive/negative" stiamo includendo sia c che idk
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """


def vader_PN_c():
    """
    ####################################################################################################
    # qui facciamo metrics su "vader", cioè considerando solo score "positive/negative"## considerando solo c
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """
    # TP = 0
    # FP = 0
    # TN = 0
    # FN = 0
    #
    # for i in df_list_true:
    #     if i[3] == 'c':
    #         if i[8] == 1:
    #             if i[1] == 1:
    #                 TP += 1
    #             else:
    #                 FP += 1
    #
    #         elif i[8] == -1:
    #             if i[1] == 1:
    #                 FN += 1
    #             else:
    #                 TN += 1
    #
    # print("TN:", TN," TP:", TP," FP:", FP," FN: ", FN)
    #
    # # Precision Score = TP / (FP + TP)
    # precision_score = TP / (FP + TP)
    # print("vader METRICS, solo c")
    # print("precision score: ", precision_score)
    #
    # # Recall Score = TP / (FN + TP)
    # recall_score = TP / (FN + TP)
    # print("recall score: ", TP / (FN + TP))
    #
    # # F1 Score = 2* Precision Score * Recall Score/ (Precision Score + Recall Score)
    # print("f1-score: ", 2*precision_score*recall_score / (precision_score + recall_score))
    #
    # # Accuracy Score = (TP + TN)/ (TP + FN + TN + FP)
    # accuracy_score = (TP + TN)/ (TP + FN + TN + FP)
    # print("accuracy score: ", accuracy_score, "\n")
    """
    ####################################################################################################
    # qui facciamo metrics su "vader ", cioè considerando solo score "positive/negative" considerando solo c
    # stiamo usano sentiment_of_tweet_PN
    ####################################################################################################
    """


def metrics_ternary_c():
    """
    solo c
    """

    actual_PNN = [x[1] for x in dataset if x[2] == 'c']
    vader_PNN = [x[3] for x in dataset if x[2] == 'c']
    roberta_PNN = [x[4] for x in dataset if x[2] == 'c']

    print("confusion metrics vader")
    print("len: ", len(vader_PNN))
    confusion_matrix_vaderPN = confusion_matrix(actual_PNN, vader_PNN, labels=[-1,0,1])
    print(confusion_matrix_vaderPN) #stampa diversa rispetto alle altre.
    print("pricision_score:", precision_score(actual_PNN, vader_PNN, labels=[-1,0,1], average=None))
    print("pricision_score weighted:", precision_score(actual_PNN, vader_PNN, labels=[-1,0,1], average='weighted'))
    print("recall:", recall_score(actual_PNN, vader_PNN, labels=[-1,0,1], average=None))
    print("accuracy:", accuracy_score(actual_PNN, vader_PNN))
    print("f1:", f1_score(actual_PNN, vader_PNN, labels=[-1,0,1], average='weighted'))
    print("\n")


    print("confusion metrics roberta")
    print("len: ", len(roberta_PNN))
    print(confusion_matrix(actual_PNN, roberta_PNN, labels=[-1,0,1]))
    print("pricision_score:", precision_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average=None))
    print("pricision_score:", precision_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average='weighted'))
    print("recall:", recall_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average=None))
    print("accuracy:", accuracy_score(actual_PNN, roberta_PNN))
    print("f1:", f1_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average='weighted'))
    print("\n")
    print("-------------------------------------------------------\n\n")
    """
    solo c
    """


def metrics_ternary_c_idk():
    """
    sia c che idk
    """

    # actual_PNN = [x[1] for x in df_list_true]
    # vader_PNN = [x[3] for x in df_list_true]
    # roberta_PNN = [x[4] for x in df_list_true]
    #
    # print("confusion metrics vader c e idk")
    # print(confusion_matrix(actual_PNN, vader_PNN, labels=[-1,0,1]))
    # print("pricision_score:", precision_score(actual_PNN, vader_PNN, labels=[-1,0,1], average=None))
    # print("pricision_score:", precision_score(actual_PNN, vader_PNN, labels=[-1,0,1], average='weighted'))
    # print("recall:", recall_score(actual_PNN, vader_PNN, labels=[-1,0,1], average=None))
    # print("accuracy:", accuracy_score(actual_PNN, vader_PNN))
    # print("f1:", f1_score(actual_PNN, vader_PNN, labels=[-1,0,1], average='weighted'))
    # print("\n")
    #
    #
    # print("confusion metrics roberta c e idk")
    # print(confusion_matrix(actual_PNN, roberta_PNN, labels=[-1,0,1]))
    # print("pricision_score:", precision_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average=None))
    # print("pricision_score:", precision_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average='weighted'))
    # print("recall:", recall_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average=None))
    # print("accuracy:", accuracy_score(actual_PNN, roberta_PNN))
    # print("f1:", f1_score(actual_PNN, roberta_PNN, labels=[-1,0,1], average='weighted'))
    # print("\n")



    """
    sia c che idk
    """


def mcc_sentiment_c():

    # prendo le predizioni di vader per gli indici labellati manualmente
    vader_predict = [x[3] for x in dataset if x[2] == 'c']

    # prendo le predizioni di xlm per gli indici labellati manualmente
    xlmroBERTa_predict = [x[4] for x in dataset if x[2] == 'c']

    sentiment_true = [x[1] for x in dataset if x[2] == 'c']



    MCC_vader = matthews_corrcoef(sentiment_true, vader_predict)
    MCC_xlm = matthews_corrcoef(sentiment_true, xlmroBERTa_predict)

    print("\n")
    print("MCC calcolati su tweets c")
    print("MCC vader: ", MCC_vader)
    print("MCC xlm_roBERTa: ", MCC_xlm)
    print("\n")
    return


def mcc_sentiment_c_idk():
    # prendo le predizioni di vader per gli indici labellati manualmente
    vader_predict = [x[3] for x in dataset]

    # prendo le predizioni di xlm per gli indici labellati manualmente
    xlmroBERTa_predict = [x[4] for x in dataset]

    sentiment_true = [x[1] for x in dataset]

    MCC_vader = matthews_corrcoef(sentiment_true, vader_predict)
    MCC_xlm = matthews_corrcoef(sentiment_true, xlmroBERTa_predict)

    print("\n")
    print("MCC calcolati su tweets c e idk")
    print("MCC vader: ", MCC_vader)
    print("MCC xlm_roBERTa: ", MCC_xlm)
    print("\n")
    return


def pie_chart(model):

    if model == "vader":
        sentiment_counts = df_predicted_complete.groupby(['vader_SCORE_pnn_numeric']).size()
        print(sentiment_counts)

        # Let's visualize the sentiments
        fig = plt.figure(figsize=(6,6), dpi=100)
        ax = plt.subplot(111)
        sentiment_counts.plot.pie(ax=ax, autopct='%1.1f%%', startangle=270, fontsize=12, label="")
        plt.show()

    elif model == "xlm_roBERTa":
        sentiment_counts = df_predicted_complete.groupby(['xlm_roberta_SCORE_numeric']).size()
        print(sentiment_counts)

        # Let's visualize the sentiments
        fig = plt.figure(figsize=(6, 6), dpi=100)
        ax = plt.subplot(111)
        sentiment_counts.plot.pie(ax=ax, autopct='%1.1f%%', startangle=270, fontsize=12, label="")
        plt.show()
    return


def emoatlasResultAnalysis():
    nomeCorrenteDataset = "all_at_21_05_2023"
    df = pd.read_csv(f'../../data/sentiment_analysis/emoatlas/{nomeCorrenteDataset}.csv')
    df = df.head(10)

    index = 0
    for i in df['emoatlas_IT']:
        print("tweets number:", index)
        current_emoatlas_scores = ast.literal_eval(i)

        for j in current_emoatlas_scores.items():
            print(j)
        index += 1
        print("\n")
    return

# metrics_ternary_c()
# mcc_sentiment_c()
# mcc_sentiment_c_idk()
# pie_chart("xlm_roBERTa")
# pie_chart("vader")
emoatlasResultAnalysis()