import pandas as pd



nomeCorrenteDataset = "all_at_21_05_2023"
df = pd.read_csv(f'../data/sentiment_analysis/emoatlas/{nomeCorrenteDataset}.csv', index_col=[0])
# df = df.head(10)


def check_howmany_two(df):
    # dictionary = eval(df['emoatlas'])
    howMany = 0
    for index, dictionaryStr in df['emoatlas_IT'].iteritems():
        dictionary = eval(dictionaryStr)
        cont = True
        for key, value in dictionary.items():
            if value == 2 or value == 0:
                cont = False
        if cont == False:
            howMany += 1
            # print(index)

    print(howMany)


check_howmany_two(df)