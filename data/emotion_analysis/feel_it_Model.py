from feel_it import EmotionClassifier, SentimentClassifier
import pandas as pd





#definiamo il dataset sul quale operare
nomeCorrenteDataset = "dataset_14_03_2023"
df = pd.read_csv(f'../data/original_dataset/{nomeCorrenteDataset}.csv')
df = df.head(10)

#creiamo la colonna nella quale metteremo la sentiment analisi con feel-it
df["feel-it_SA"] = " "
emotion_classifier = EmotionClassifier()

#print(str(df.text_clean))
for tweet in df.text:
    tweet_index = df[df['text'] == tweet].index.values.astype(int)[0]
    df.at[tweet_index, 'feel-it_SA'] = emotion_classifier.predict([str(tweet)])


print(df)