import tweepy
from tweepy import OAuthHandler
import time
import pandas as pd
from querys import querys


def fetch_tweets(query):

    client = tweepy.Client(
        bearer_token='AAAAAAAAAAAAAAAAAAAAAJHybgEAAAAAE1sIR8%2F1yZIpEd7%2BGQ5O2m%2Bcxmo'
                     '%3D2f9ke2oq3YWluyzgpdfAghKY4xqwRPx3jL1FhKkzqZs63vBiaN',
        consumer_key='8nwz0CeOTx0diZah5xOErvxBg',
        consumer_secret='CtbpS6RcuAlkyZuAcvwIRx9cZQssvolRRePpCWRBrHIYIiELvg',
        access_token='1517109136479117313-oKEm4Hf27OfguYvOXavXCxaPpVep3n',
        access_token_secret='oqA9HPYwxU7bnSZA268cQm8EHOWnHCDvgPNi2e9nO8LI8')


    tweets = tweepy.Paginator(client.search_recent_tweets, query=query,
                              tweet_fields=['text', 'created_at'],
                              max_results=100
                              ).flatten(limit=1000)

    return tweets


if __name__ == '__main__':

    pd.set_option("max_colwidth", None)

    current_query_tweets = fetch_tweets(querys[0])
    df = pd.DataFrame(current_query_tweets, columns=['text', 'created_at'])


    for query in querys[1:]:
        current_query_tweets = fetch_tweets(query)
        df = pd.concat([df, pd.DataFrame(current_query_tweets, columns=['text', 'created_at'])])

    #remove duplicate
    df.drop_duplicates(inplace=True)
    df.reset_index(drop=True, inplace=True)

    nomeCorrenteDataset = "dataset_" + time.strftime("%d_%m_%Y")
    #df.drop(df.columns[0], axis=1, inplace=True)
    df.to_csv(f'data/original_dataset/{nomeCorrenteDataset}.csv') # questo percorso funziona correttamente se questo file è eseguito tramite il main, se esegui il file singolo nella directory get_tweets allora non funziona!

