
querys = [
        '-is:retweet lang:it transizione verde',
        '-is:retweet lang:it rivoluzione verde',
        '-is:retweet lang:it transizione green',
        '-is:retweet lang:it transizione verde #green',
        '-is:retweet lang:it transizione energetica',
        '-is:retweet lang:it transizione ecologica',
        '-is:retweet lang:it transizione ecologica climate change',
        '-is:retweet lang:it rivoluzione ecologica',
        '-is:retweet lang:it #transizionEcologica',
        '-is:retweet lang:it #transizione #ecologica'
    ]

word_in_all_querys = ['transizione',
                      'verde',
                      'rivoluzione',
                      'green',
                      '#green',
                      'energetica',
                      'ecologica',
                      'climate',
                      'change',
                      '#transizionEcologica',
                      '#transizione',
                      '#ecologica'
                      ]
word_in_all_querys_en = ['transition',
                      'green',
                      'revolution',
                      '#green',
                      'energy',
                      'ecological',
                      'climate',
                      'change',
                      '#transition',
                      '#ecologic'
                      ]