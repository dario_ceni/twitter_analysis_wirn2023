# il modello sembra funzionare, tuttavia non ho valutato i risultati.
#https://towardsdatascience.com/lda-topic-modeling-with-tweets-deff37c0e131

# oppure
# https://techblog.smc.it/it/2021-10-22/topic-modelling

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from gensim.corpora import Dictionary
from gensim.models.ldamodel import LdaModel
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
pd.set_option('display.min_rows', 50)
pd.options.display.max_colwidth = 150


#definiamo il dataset sul quale operare
nomeCorrenteDataset = "dataset_21_03_2023"
df = pd.read_csv(f'../data/clear_dataset/{nomeCorrenteDataset}.csv', index_col=[0])


all_words = [word for tokens in df['translated_tweets_and_clean_WO_querywords'] for word in tokens]
tweet_lengths = [len(tokens) for tokens in df['translated_tweets_and_clean_WO_querywords']]
vocab = sorted(list(set(all_words)))

#print('{} tokens total, with a vocabulary size of {}'.format(len(all_words), len(vocab)))
#print('Max tweet length is {}'.format(max(tweet_lengths)))


word_length = []
for word in all_words:
    word_length.append(len(word))

#print('average word size is {}'.format( sum(word_length) / len(word_length)))


plt.figure(figsize = (15,8))
sns.countplot(tweet_lengths)
plt.title('Tweet Length Distribution', fontsize = 18)
plt.xlabel('Words per Tweet', fontsize = 12)
plt.ylabel('Number of Tweets', fontsize = 12)
plt.savefig("../data/topic_analysis/dataset_21_03_2023/lda")


#eliminiamo tweet corti
# less_than_3_tokens = df[df['translated_tweets_and_clean_WO_querywords'].apply(lambda x: len(x) <= 3)].index
# df.drop(less_than_3_tokens, inplace = True)


#tramite gensim l'idea è di mappare le parole su numeri (int) in questo modo il modello userà numeri e non parole.
# text_dict = Dictionary(df.text_clean_TokAndLem)
# text_dict.token2id

#print(text_dict)


# tweets_bow = [text_dict.doc2bow(tweet) for tweet in df['translated_tweets_and_clean_WO_querywords']]
#
# k = 5
# tweets_lda = LdaModel(tweets_bow,
#                       num_topics = k,
#                       id2word = text_dict,
#                       random_state = 1,
#                       passes=10)
#
# print(tweets_lda.show_topics())


#da sistemare