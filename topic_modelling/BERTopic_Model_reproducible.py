# Data processing
import pandas as pd
import numpy as np
# Text preprocessiong
import nltk
# nltk.download('stopwords')
# nltk.download('omw-1.4')
# nltk.download('wordnet')
# wn = nltk.WordNetLemmatizer()
# Topic model
from bertopic import BERTopic
# Dimension reduction
from umap import UMAP



#definiamo il dataset sul qua  le operare
nomeCorrenteDataset = "last_week_data"
df = pd.read_csv(f'../data/complete_dataset/{nomeCorrenteDataset}.csv', index_col=[0])
# df = df.head(200)


def topic_modelling():
    df["BERTopic_prediction"] = " "
    tweets = df.text_clean_EN_TokAndLem_WO_query_words_WO_stopwords.to_list()
    tweets = [str(x) for x in tweets]


    ####################################################################################################################
    # Se è la prima volta e si vuole creare il modello
    # Initiate UMAP
    umap_model = UMAP(n_neighbors=15,
                      n_components=5,
                      min_dist=0.0,
                      metric='cosine',
                      random_state=100)

    # Initiate BERTopic
    topic_model = BERTopic(umap_model=umap_model, language="english", calculate_probabilities=True, nr_topics=10)

    ####################################################################################################################

    # se già creato e salvato precedentemente un modello usiamo questo
    # topic_model= BERTopic.load("modelli_bertopic_salvati/all_at_21_05_2023")

    # Run BERTopic model
    topics, probabilities = topic_model.fit_transform(tweets)


    # Get the list of topics
    print("topic model info PRINT: ", topic_model.get_topic_info())

    # Get top 10 terms for a topic
    # print(topic_model.get_topic(0))


    fig = topic_model.visualize_barchart(n_words=6,top_n_topics=10)
    fig.write_html(f"../data/topic_analysis/last_week_data/barchart.html")


    # print words relative importance for each topic
    topics_words = []
    for i in range(10):
        topics_words.append(topic_model.get_topic(i))

    print("topic words PRINT:", topics_words)

    # Save the chart to a variable # l'indice 185 è da verificare ma dovrebbe corrispondere all'indice del tweet passato.
    # chart = topic_model.visualize_distribution(topic_model.probabilities_[185])
    # Write the chart as a html file
    # chart.write_html(f"../data/topic_analysis/dataset_from_06_03_to_02_04/{nomeCorrenteDataset}distribution.html")


    # Get the topic predictions for the tweets of index "n"
    # topic_prediction = topic_model.topics_[2]
    # print(topic_prediction)

    # Get the topic predictions
    topic_prediction = topic_model.topics_[:]
    # Save the predictions in the dataframe
    df['BERTopic_prediction'] = topic_prediction

    # modo opzionale per salvare topic funziona (non so)?
    # for i in range(len(df)):
    #     df.loc[i, 'BERTopic_prediction'] = topic_model.topics_[i]




    # Save the predictions in the dataframe
    # df['BERTopic_prediction'] = topic_prediction

    # salvare il modello per run future
    topic_model.save(f"modelli_bertopic_salvati/{nomeCorrenteDataset}")

    df.to_csv(f'{nomeCorrenteDataset}.csv')


def metrics_on_topic():
    nomeCorrenteDataset = "lastMonth_data"
    df = pd.read_csv(f'../data/topic_analysis/lastMonth_data/{nomeCorrenteDataset}.csv')
    df_analysis = df[['BERTopic_prediction', 'vader_SCORE_pnn_numeric', 'xlm_roberta_SCORE_numeric']]
    df_metrics = []

    # avevamo settato a 10 il numero di topic, di fatto il primo topic "0" contiene tutti i tweet privi di topic, per questo scriviamo a seguire numero topic = 8
    n_topics = 9
    metrics_by_topic = []

    for i in range(n_topics):
        df_metrics_one_topic = []
        number_of_tweets_by_topic = 0

        # metrics è una lista di 3 elementi dove l'elemento  0 sono i tweet negativi, 1 neutri, 2 positivi
        metrics_vader = [0, 0, 0]
        metrics_xlm = [0, 0, 0]
        for j in range(len(df_analysis)):
            if df_analysis.loc[j, 'BERTopic_prediction'] == i:
                number_of_tweets_by_topic += 1
                if df_analysis.loc[j, 'vader_SCORE_pnn_numeric'] == -1:
                    metrics_vader[0] += 1
                if df_analysis.loc[j, 'vader_SCORE_pnn_numeric'] == 0:
                    metrics_vader[1] += 1
                if df_analysis.loc[j, 'vader_SCORE_pnn_numeric'] == 1:
                    metrics_vader[2] += 1

                if df_analysis.loc[j, 'xlm_roberta_SCORE_numeric'] == -1:
                    metrics_xlm[0] += 1
                if df_analysis.loc[j, 'xlm_roberta_SCORE_numeric'] == 0:
                    metrics_xlm[1] += 1
                if df_analysis.loc[j, 'xlm_roberta_SCORE_numeric'] == 1:
                    metrics_xlm[2] += 1

        df_metrics_one_topic = [i,
                                number_of_tweets_by_topic,
                                metrics_vader[0],
                                round((metrics_vader[0] / (metrics_vader[0] + metrics_vader[1] + metrics_vader[2])), 2),
                                metrics_vader[1],
                                round((metrics_vader[1]/(metrics_vader[0]+metrics_vader[1]+metrics_vader[2])), 2),
                                metrics_vader[2],
                                round((metrics_vader[2]/(metrics_vader[0]+metrics_vader[1]+metrics_vader[2])), 2),
                                metrics_xlm[0],
                                round((metrics_xlm[0] / (metrics_xlm[0] + metrics_xlm[1] + metrics_xlm[2])), 2),
                                metrics_xlm[1],
                                round((metrics_xlm[1] / (metrics_xlm[0] + metrics_xlm[1] + metrics_xlm[2])), 2),
                                metrics_xlm[2],
                                round((metrics_xlm[2] / (metrics_xlm[0] + metrics_xlm[1] + metrics_xlm[2])), 2)
                                ]

        print("topic:", i, ", numero di tweets:",number_of_tweets_by_topic)
        print("vader: ")
        print("n° tweet negativi: ", metrics_vader[0], "(",round((metrics_vader[0]/(metrics_vader[0]+metrics_vader[1]+metrics_vader[2])), 2),")")
        print("n° tweet neutri: ", metrics_vader[1],  "(",round((metrics_vader[1]/(metrics_vader[0]+metrics_vader[1]+metrics_vader[2])), 2),")")
        print("n° tweet positivi: ", metrics_vader[2],  "(",round((metrics_vader[2]/(metrics_vader[0]+metrics_vader[1]+metrics_vader[2])), 2),")")

        print("\n")
        print("xlm: ")
        print("n° tweet negativi: ", metrics_xlm[0], "(", round((metrics_xlm[0] / (metrics_xlm[0] + metrics_xlm[1] + metrics_xlm[2])), 2), ")")
        print("n° tweet neutri: ", metrics_xlm[1], "(", round((metrics_xlm[1] / (metrics_xlm[0] + metrics_xlm[1] + metrics_xlm[2])), 2), ")")
        print("n° tweet positivi: ", metrics_xlm[2], "(", round((metrics_xlm[2] / (metrics_xlm[0] + metrics_xlm[1] + metrics_xlm[2])), 2), ")")
        print("\n")

        df_metrics.append(df_metrics_one_topic)

    print(df_metrics)

    # qui si vuole creare un dataframe in previsione di un'esportazione in csv, mostrare in una tabella,
    # per ciascun topic i relativi tweets e proporzioni di positivi/negativi/neutri.
    df_metrics_export = pd.DataFrame(df_metrics, columns=['topic',
                                                          'number_of_tweet_in_topic',
                                                          'vader_number_neg',
                                                          'vader_neg_percentage',
                                                          'vader_number_neu',
                                                          'vader_neu_percentage',
                                                          'vader_number_pos',
                                                          'vader_pos_percentage',
                                                          'xlm_number_neg',
                                                          'xlm_neg_percentage',
                                                          'xlm_number_neu',
                                                          'xlm_neu_percentage',
                                                          'xlm_number_pos',
                                                          'xlm_pos_percentage',
                                                          ])

    df_metrics_export.to_csv(f'../data/topic_analysis/{nomeCorrenteDataset}/topic_metrics.csv')

    return


topic_modelling()
# metrics_on_topic()
